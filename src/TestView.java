import org.assertj.swing.fixture.FrameFixture;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import es.ull.pai.practica8.controller.Controller;

public class TestView {
	/*
	 * We create a FrameFixture, which defines a Robot
	 * object in order to: (1) simulate human interaction
	 * and (2) access components by name.
	 */
	private FrameFixture frame;
	
	private Controller objController = new Controller(10, 20);
	
	/* Before running our tests, we have to
	 * initilize the object FrameFixture
	 */
	@Before
	public void setUp() {
		frame = new FrameFixture(objController.getBallWindow());
	}
	
	@Test
	public void shouldChangeTextInTextFieldWhenClickingButton() {
		frame.button()
		frame.label("").requireText("Button not pressed");
		frame.button("myButton").click(); // function that uses Robot
		frame.label("myLabel").requireText("Button pressed!");
	}
	
	/*
	 * Release resources used by Robot
	 * (keyboard, mouse, windows, ...)
	 */
	@After
	public void clean() {
		frame.cleanUp();
	}
}
