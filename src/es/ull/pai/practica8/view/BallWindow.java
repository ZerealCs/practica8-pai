/**
 * 
 */
package es.ull.pai.practica8.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
 
/**
 * @author eleazardd
 *
 */
public class BallWindow extends JFrame {
	private String TITLE = "Move Ball";

	/**
	 * Create the frame.
	 */
	public BallWindow(Directions directions, Canvas canvas) {
		setTitle(TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		contentPane.add(canvas);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		panel.add(directions, BorderLayout.CENTER);
		pack();
	}

}
