/**
 * 
 */
package es.ull.pai.practica8.model;

/**
 * @author Eleazar Díaz Delgado
 * Represent a Circle, with a center point and radius, it can be movable into a
 * bound shape. All measures are in Pixels.
 */
public class Circle implements Movable {
	private int posX;			// Pos in X of center point Circle
	private int posY;			// Pos in Y of center point Circle
	private int radius;         // Radius of deteminate circle

	/**
	 * Build a Circle with specified position, and radius.
	 */
	public Circle(int x, int y, int circleRadius) {
		posX = x;
		posY = y;
		radius = circleRadius;
	}

	public void left(int steps, Bound constraints) {
		System.out.println("Izquierda");
		if (constraints.leftLimit() < (posX - radius - steps)) {
			posX -= steps;
		} else {
			posX = constraints.leftLimit() + radius;
		}
	}

	public void right(int steps, Bound constraints) {
		System.out.println("Derecha");
		if (constraints.rightLimit() > (posX + radius + steps)) {
			posX += steps;
		} else {
			posX = constraints.rightLimit() - radius;
		}
	}

	public void up(int steps, Bound constraints) {
		System.out.println(posY - radius - steps);

		if (constraints.upLimit() < (posY - radius - steps)) {
			posY -= steps;
		} else {
			posY = constraints.upLimit() + radius;
		}
	}

	public void down(int steps, Bound constraints) {
		System.out.println("Down");
		System.out.println(posY + radius + steps);

		if (constraints.downLimit() > (posY + radius + steps)) {
			posY += steps;
		} else {
			posY = constraints.downLimit() - radius;
		}
	}

	/**
	 * Adjust circle to constraints if circle is greater than constraints, that
	 * not works well.
	 */
	public void recalculate(Bound constraints) {
		left(0, constraints);
		right(0, constraints);
		up(0, constraints);
		down(0, constraints);
	}

	/**
	 * @return the posX
	 */
	public int getPosX() {
		return posX;
	}

	/**
	 * @param posX the posX to set
	 */
	public void setPosX(int posX) {
		this.posX = posX;
	}

	/**
	 * @return the posY
	 */
	public int getPosY() {
		return posY;
	}

	/**
	 * @param posY the posY to set
	 */
	public void setPosY(int posY) {
		this.posY = posY;
	}

	/**
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}

	/**
	 * @param radius the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}
}
